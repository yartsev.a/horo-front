import https from 'https';
import camelcaseKeys from 'camelcase-keys';
import snakecaseKeys from 'snakecase-keys';
import qs from 'qs';

export default function (context) {
  let { $axios, res, app, route, $config } = context;

  $axios.defaults.httpsAgent = new https.Agent({ rejectUnauthorized: false });

  $axios.onRequest((config) => {
    if (process.server) {
      config.headers.referer = $config.app.url + route.path;
    }

    config.headers['Accept-Language'] = app.store.state.i18n.locale;
  });

  $axios.onResponse((axiosResponse) => {
    if (process.server && axiosResponse.headers['set-cookie']) {
      // console.log('[axios] set-cookie', axiosResponse.headers['set-cookie']);
      res.setHeader('set-cookie', (res.getHeader('set-cookie') || []).concat(axiosResponse.headers['set-cookie']));
    }
  });

  $axios.interceptors.request.use(
    (config) => {
      if (config.params) {
        config.params = snakecaseKeys(config.params, { deep: true });
      }

      if (config.data && !(config.data instanceof FormData)) {
        config.data = snakecaseKeys(config.data, { deep: true });
      }

      config.paramsSerializer = (params) => {
        return qs.stringify(params, {
          arrayFormat: 'brackets',
          encode: false,
        });
      };

      return config;
    },
    (error) => {
      throw error;
    }
  );

  $axios.interceptors.response.use(
    (response) => {
      if (response.data) {
        response.data = camelcaseKeys(response.data, {
          deep: true,
          stopPaths: ['invalid_fields'],
        });
      }

      return response;
    },
    async (error) => {
      if (!error.isAxiosError) {
        throw error;
      }

      if (error.response) {
        error.response.data = camelcaseKeys(error.response.data, {
          deep: true,
          stopPaths: ['invalid_fields'],
        });
      }

      let logError = (error) => {
        console.error(
          JSON.stringify(
            {
              ...error.response.data,
              trace: error.response.data.trace && error.response.data.trace.slice(0, 5),
            },
            null,
            '\t'
          )
        );
      };

      if (error.config.doNotRefreshSession) {
        throw error;
      }

      if (error.response && ['Unauthenticated', 'CSRF Token Mismatch'].includes(error.response.data.error)) {
        try {
          let response = await $axios.get('auth', {
            doNotRefreshSession: true,
          });

          if (error.response.data.error === 'Unauthenticated' && !response.data.data.user) {
            throw error;
          }
        } catch (authError) {
          logError(error);
          throw error;
        }

        console.log(error.config);

        return $axios.request({
          ...error.config,
          data:
            process.client && error.config.data instanceof FormData
              ? error.config.data
              : error.config.data && JSON.parse(error.config.data),
          doNotRefreshSession: true,
        });
      }

      error.response && logError(error);
      throw error;
    }
  );
}
