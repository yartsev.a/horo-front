import Vue from 'vue';
import GAuth from 'vue-google-oauth2';

Vue.use(GAuth, {
  clientId: '801794139174-dgsh8frcngm6g4mkkq71ritl8bjj0067.apps.googleusercontent.com',
  scope: 'profile email',
  prompt: 'select_account consent',
});
