export default function ({ store, redirect, localePath }) {
  if (!store.state.auth.user) {
    return redirect(localePath({ name: 'index' }));
  }

  //
}
