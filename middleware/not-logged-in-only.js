export default function ({ store, redirect, localePath }) {
  if (!store.state.auth.user) {
    return;
  }

  if (store.state.auth.onboarding.step) {
    return redirect(
      localePath({
        name: `onboarding.${store.state.auth.onboarding.step.toLowerCase().replace(/_/g, '-')}`,
      })
    );
  }

  return redirect({ name: 'today' });
}
