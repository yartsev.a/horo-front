export default function ({ app, redirect, route, localePath }) {
  console.log('[checkEntryPointRoute] ...');

  if (!route.name) {
    return;
  }

  let authUser = app.store.state.auth.user;
  let onboardingRoute = app.store.getters.onboardingRoute;
  let [currentRouteName, locale] = route.name.split('___');
  locale = locale || app.store.state.i18n.locale;
  let currentRouteNameParts = currentRouteName.split(/[.]/);
  let entryPointRoute = app.store.getters.entryPointRoute;
  let entryPointRouteParts = entryPointRoute.split(/[.]/);

  if (currentRouteNameParts[0] === 'index') {
    if (authUser) {
      console.log(`Redirecting to: ${entryPointRoute} [1]`);

      return redirect(onboardingRoute ? localePath({ name: onboardingRoute }) : { name: entryPointRoute });
    }

    console.log('[checkEntryPointRoute] index, OK');
    return;
  }

  // if (currentRouteNameParts[0] === 'index') {
  //   if (authUser) {
  //     console.log(`Redirecting to: index___${locale} [2]`);
  //     return redirect({ name: `index___${locale}` });
  //   }

  //   if (currentRouteName !== entryPointRoute) {
  //     console.log(`Redirecting to: ${entryPointRoute} [3]`, currentRouteName);
  //     return redirect({ name: entryPointRoute });
  //   }

  //   console.log('[checkEntryPointRoute] onboarding, OK');
  //   return;
  // }

  // if (currentRouteNameParts[0] === 'onboarding') {
  //   if (!authUser) {
  //     console.log(`Redirecting to: index___${locale} [2]`);
  //     return redirect({ name: `index___${locale}` });
  //   }

  //   if (currentRouteName !== entryPointRoute) {
  //     console.log(`Redirecting to: ${entryPointRoute} [3]`, currentRouteName);
  //     return redirect({ name: entryPointRoute });
  //   }

  //   console.log('[checkEntryPointRoute] onboarding, OK');
  //   return;
  // }

  // if (currentRouteNameParts[0] === 'preparing') {
  //   if (!authUser) {
  //     console.log(`Redirecting to: index___${locale} [2]`);
  //     return redirect({ name: `index___${locale}` });
  //   }

  //   if (currentRouteName !== entryPointRoute) {
  //     console.log(`Redirecting to: ${entryPointRoute} [3]`, currentRouteName);
  //     return redirect({ name: entryPointRoute });
  //   }

  //   console.log('[checkEntryPointRoute] preparing, OK');
  //   return;
  // }

  // if (['today'].includes(currentRouteNameParts[0])) {
  //   if (!authUser) {
  //     console.log(`Redirecting to: index___${locale} [4]`);
  //     return redirect({ name: `index___${locale}` });
  //   }

  //   if (currentRouteNameParts[0] !== entryPointRouteParts[0]) {
  //     console.log(`Redirecting to: ${entryPointRoute} [5]`, currentRouteNameParts[0]);
  //     return redirect({ name: entryPointRoute });
  //   }

  //   console.log('[checkEntryPointRoute] dashboard, OK');
  //   return;
  // }

  console.log(`[checkEntryPointRoute] OK, ${currentRouteNameParts[0]}`);
}
