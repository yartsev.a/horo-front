process.env.VERSION = process.env.npm_package_version;
process.env.APP_ENV = process.env.APP_ENV || (process.env.NODE_ENV === 'production' ? 'production' : 'local');
const isDevelopment = process.env.APP_ENV === 'development';
const isLocal = process.env.APP_ENV === 'local';
const isProduction = process.env.APP_ENV === 'production';
process.env.APP_URL =
  process.env.APP_URL || `https://${process.env.APP_ENV === 'production' ? 'www' : 'dev'}.daily-horoscope.pro`;
process.env.API_URL = process.env.API_URL || `${process.env.APP_URL}/api/v1`;
process.env.API_PROXY = process.env.API_PROXY ? process.env.API_PROXY : isLocal ? 'true' : 'false';

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'horoscope',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
      { name: 'apple-mobile-web-app-status-bar-style', content: 'black-translucent' },
      { name: 'facebook-domain-verification', content: 'idakqosqwkn3rqkebtfyej3dknva8r' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    script: [
      { src: 'https://js.stripe.com/v3/' },
      {
        innerHTML: `
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '369726608168827');
          fbq('track', 'PageView');
        `,
      },
      {
        innerHTML: `
          window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
          ga('create', 'UA-209697073-1', 'auto');
          ga('send', 'pageview');
        `,
      },
      { src: 'https://www.google-analytics.com/analytics.js', async: true },
    ],
    __dangerouslyDisableSanitizers: ['script'],
  },

  loading: {
    color: '#961937',
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~assets/styles/main.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~plugins/interface',
    '~plugins/axios',
    { src: '~plugins/vue-google-oauth2', mode: 'client' },
    '~plugins/moment',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    ['@nuxtjs/eslint-module', { fix: true }],
    // https://go.nuxtjs.dev/stylelint
    ['@nuxtjs/stylelint-module', { fix: true }],
    // https://google-fonts.nuxtjs.org
    '@nuxtjs/google-fonts',
    // https://www.npmjs.com/package/@nuxtjs/moment
    '@nuxtjs/moment',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://www.npmjs.com/package/@nuxtjs/style-resources
    '@nuxtjs/style-resources',
    // https://i18n.nuxtjs.org/
    [
      'nuxt-i18n',
      {
        detectBrowserLanguage: {
          alwaysRedirect: true,
          cookieKey: 'locale',
          useCookie: true,
          onlyOnNoPrefix: true,
        },
        seo: false,
        locales: ['cs', 'da', 'de', 'en', 'es', 'fr', 'it', 'pl', 'pt', 'ru', 'uk'].map((locale) => {
          return {
            code: locale,
            file: `${locale}.json`,
            iso: locale,
          };
        }),
        lazy: true,
        langDir: 'locales/',
        defaultLocale: 'en',
        strategy: 'prefix',
        fallbackLocale: 'en',
        formatFallbackMessages: true,
        vuex: { syncLocale: true },
        vueI18n: { silentTranslationWarn: true, silentFallbackWarn: true },
      },
    ],
    'nuxt-google-optimize',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: process.env.API_URL,
    browserBaseURL: new URL(process.env.API_URL).pathname,
    proxy: process.env.API_PROXY !== 'false',
  },

  moment: {
    defaultLocale: 'en',
    locales: ['ru'],
  },

  // Proxy configuration (https://go.nuxtjs.dev/config-axios#proxy)
  proxy: {
    [new URL(process.env.API_URL).pathname]: {
      target: new URL(process.env.API_URL).origin,
      secure: false,
      headers: { Referer: process.env.APP_URL },
    },
  },

  // Router configuration (https://nuxtjs.org/docs/2.x/configuration-glossary/configuration-router/)
  router: {
    routeNameSplitter: '.',
    // middleware: ['check-route'],
  },

  styleResources: {
    scss: ['~/assets/styles/helpers.scss'],
  },

  googleFonts: {
    families: {
      Roboto: [100, 400, 700],
    },
    prefetch: true,
  },

  googleOptimize: {
    experimentsDir: '~/experiments',
    // maxAge: 60 * 60 * 24 * 7 // 1 Week
    // pushPlugin: true,
    // excludeBots: true,
    // botExpression: /(bot|spider|crawler)/i
  },

  server: {
    host: process.env.APP_HOST || '0.0.0.0',
    port: process.env.APP_PORT || 3000,
  },

  telemetry: false,

  privateRuntimeConfig: {
    app: {
      url: process.env.APP_URL,
    },
  },

  vue: {
    config: {
      productionTip: true,
      devtools: !isProduction,
      silent: isProduction,
      performance: true,
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    devtools: !isProduction,

    filenames: {
      app: () => (isLocal ? '[name].js' : isDevelopment ? '[name].[contenthash].js' : '[contenthash].js'),
      chunk: () => (isLocal ? '[name].js' : isDevelopment ? '[name].[contenthash].js' : '[contenthash].js'),
      css: () => (isLocal ? '[name].css' : isDevelopment ? '[name].[contenthash].css' : '[contenthash].css'),

      img: () =>
        isLocal
          ? '[path][name].[ext]'
          : isDevelopment
          ? 'img/[name].[contenthash:7].[ext]'
          : 'img/[contenthash:7].[ext]',

      font: () =>
        isLocal
          ? '[path][name].[ext]'
          : isDevelopment
          ? 'fonts/[name].[contenthash:7].[ext]'
          : 'fonts/[contenthash:7].[ext]',

      video: () =>
        isLocal
          ? '[path][name].[ext]'
          : isDevelopment
          ? 'videos/[name].[contenthash:7].[ext]'
          : 'videos/[contenthash:7].[ext]',
    },

    optimization: { minimize: isProduction },
    transpile: ['d3', 'internmap', 'delaunator', 'robust-predicates'],

    /*
     ** You can extend webpack config here
     */
    extend(config, context) {
      config.module.rules.push({
        test: /\.(ogg|mp3|wav|mpe?g)$/i,
        loader: 'file-loader',
        options: { name: '[path][name].[ext]' },
      });
    },
  },
};
