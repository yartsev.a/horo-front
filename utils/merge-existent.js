import { intersection, keys, isObject, isArray } from 'lodash';

export default function mergeExistent(original, merging) {
  let usingKeys = intersection(keys(original), keys(merging));

  for (let usingKey of usingKeys) {
    if (
      isObject(original[usingKey]) &&
      !isArray(original[usingKey]) &&
      isObject(merging[usingKey]) &&
      !isArray(merging[usingKey])
    ) {
      mergeExistent(original[usingKey], merging[usingKey]);
      continue;
    }

    original[usingKey] = merging[usingKey];
  }

  return original;
}
