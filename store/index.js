import mergeExistent from '~/utils/merge-existent.js';
import { debounce } from 'lodash';

export default {
  state() {
    return {
      auth: {
        user: null,
        email: null,
        emailChecker: { cancel: null, isLoading: false },
        onboarding: null,
        stripe: null,
      },
    };
  },

  getters: {
    onboardingRoute(state) {
      if (!state.auth.onboarding.step) {
        return null;
      }

      return `onboarding.${state.auth.onboarding.step.toLowerCase().replace(/_/g, '-')}`;
    },

    entryPointRoute(state) {
      if (!state.auth.user) {
        return 'index';
      }

      if (state.auth.user.entryPoint === 'DASHBOARD') {
        return 'dashboard.index';
      }

      return state.auth.user.entryPoint.toLowerCase().replace(/_/, '.').replace(/_/g, '-');
    },
  },

  actions: {
    async nuxtServerInit({ commit }, { route }) {
      console.log('[nuxtServerInit] [start]', route.path);
      let response = await this.$axios.get('auth');
      // console.log('[nuxtServerInit] [data]', response.data);
      commit('SET_AUTH', response.data.data);
      console.log('[nuxtServerInit] [end]', route.path);
    },

    async checkEmail({ dispatch, commit }, email) {
      dispatch('cancelEmailChecking');
      commit('SET_AUTH_EMAIL_CHECKER_IS_LOADING', !!email);
      !email && commit('SET_AUTH_EMAIL', null);
      dispatch('checkEmailDebounce', email);
    },

    checkEmailDebounce: debounce(async function ({ commit }, email) {
      if (!email) {
        return;
      }

      try {
        let response = await this.$axios.post(
          '/auth/email/check',
          { user: { email } },
          {
            cancelToken: new this.$axios.CancelToken((cancel) => {
              commit('SET_AUTH_EMAIL_CHECKER_CANCEL', cancel);
            }),
          }
        );

        commit('SET_AUTH_EMAIL', response.data.data);
        commit('SET_AUTH_EMAIL_CHECKER_IS_LOADING', false);
      } catch (error) {
        if (this.$axios.isCancel(error)) {
          return;
        }

        commit('SET_AUTH_EMAIL_CHECKER_IS_LOADING', false);
        console.error(error, Vue);
      }
    }, 250),

    cancelEmailChecking({ state, commit }) {
      state.auth.emailChecker.cancel && state.auth.emailChecker.cancel();
      commit('SET_AUTH_EMAIL_CHECKER_CANCEL', null);
    },

    async forgetEmail({ commit }) {
      await this.$axios.post('auth/email/forget');
      commit('SET_AUTH_EMAIL', null);
    },

    async loginThroughGoogle({ commit }, data) {
      let response = await this.$axios.post('auth/google/login', data);
      commit('SET_AUTH', response.data.data);
      await this.$i18n.setLocale(response.data.data.user.locale);
    },

    async login({ commit }, data) {
      let response = await this.$axios.post('auth/login', data);
      commit('SET_AUTH', response.data.data);
      // this.$i18n.locale = response.data.data.user.locale;
      await this.$i18n.setLocale(response.data.data.user.locale);
    },

    async register({ commit }, data) {
      data.user = { ...data.user, locale: this.$i18n.locale };
      let response = await this.$axios.post('auth/register', data);
      commit('SET_AUTH', response.data.data);
      // this.$i18n.locale = response.data.data.user.locale;
      await this.$i18n.setLocale(response.data.data.user.locale);
    },

    async logout({ commit }) {
      let response = await this.$axios.post('auth/logout');
      commit('SET_AUTH', response.data.data);
    },
  },

  mutations: {
    SET_AUTH(state, data) {
      state.auth = {
        ...state.auth,
        user: data.user,
        email: data.email,
        onboarding: data.onboarding,
        stripe: data.stripe,
        isNew: data.isNew,
      };
    },

    MERGE_AUTH(state, data) {
      state.auth = mergeExistent({ ...state.auth }, data);
    },

    SET_AUTH_EMAIL(state, data) {
      state.auth = { ...state.auth, email: data };
    },

    SET_AUTH_EMAIL_CHECKER_IS_LOADING(state, data) {
      state.auth.emailChecker.isLoading = data;
    },

    SET_AUTH_EMAIL_CHECKER_CANCEL(state, cancel) {
      state.auth.emailChecker.cancel = cancel;
    },

    SET_AUTH_USER(state, data) {
      state.auth = { ...state.auth, user: data };
    },
  },
};
